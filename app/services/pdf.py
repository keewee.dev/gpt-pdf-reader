import os
import fitz  # PyMuPDF


def convert_pdf_to_image(pdf_path, page_numbers: int):
    pdf_document = fitz.open(pdf_path)
    paths = []
    for page_number in range(page_numbers):
        page = pdf_document.load_page(page_number)
        image = page.get_pixmap()
        temp_image_path = os.path.join(
            os.path.dirname(pdf_path), "temp", f"temp_{page_number}.jpg"
        )

        image._writeIMG(temp_image_path, format_="jpg", jpg_quality=95)
        paths.append(temp_image_path)
    pdf_document.close()
    return paths
