import base64
from dotenv import load_dotenv
from openai import OpenAI

import os
from typing import List

load_dotenv()
# OpenAI API Key
api_key = os.environ.get("OPENAI_API_KEY")
client = OpenAI(api_key=api_key)


# Function to encode the image
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode("utf-8")


def create_messages(
    question: str = "What's in this image?", images=[], max_tokens: int = 300
):
    messages = [{"role": "user", "content": [{"type": "text", "text": question}]}]

    for image in images:
        base64_image = encode_image(
            image
        )  # assuming you have a function to convert image to base64
        messages[0]["content"].append(
            {
                "type": "image_url",
                "image_url": {"url": f"data:image/jpeg;base64,{base64_image}"},
            }
        )

    return messages


def get_image_caption(images: List[str], question: str, max_tokens: int = 4000):

    messages = create_messages(question, images, max_tokens)
    response = client.chat.completions.create(
        model="gpt-4-vision-preview",
        messages=messages,
    )

    return response.choices[0]
