import os

from .services.caption import get_image_caption
from .services.pdf import convert_pdf_to_image

file_name = "test.pdf"
page_numbers = 3
question = "What's described in this image?"

if __name__ == "__main__":

    image_path = os.path.join(os.path.dirname(__file__), "..", "input_files", file_name)
    paths = convert_pdf_to_image(image_path, page_numbers)
    caption = get_image_caption(
        paths,
        question=question,
    )
    print(caption.message)
